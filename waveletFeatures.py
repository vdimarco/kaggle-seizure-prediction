# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

from scipy.io import loadmat
# import dtcwt
import numpy as np
from scipy.signal import butter,lfilter
from scipy import signal
import sys, getopt
# <codecell>
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

def filterBankAnalysis(data,sampRate):
    f1 = [0,4] #delta filter bank
    f2 = [4,7] #theta filter bank
    f3 = [7,13] # alpha filter bank
    f4 = [13,15] # low beta
    f5 = [14,30] # high beta
    f6 = [30,45] # low gamma
    f7 = [65, 120] #high gamma
    fbDict=dict()
    fbDict['delta'] = butter_bandpass_filter(data,f1[0],f1[1],sampRate)
    fbDict['theta'] = butter_bandpass_filter(data,f2[0],f2[1],sampRate)
    fbDict['alpha'] = butter_bandpass_filter(data,f3[0],f3[1],sampRate)
    fbDict['lbeta'] = butter_bandpass_filter(data,f4[0],f4[1],sampRate)
    fbDict['hbeta'] = butter_bandpass_filter(data,f5[0],f5[1],sampRate)
    fbDict['lgamma'] = butter_bandpass_filter(data,f6[0],f6[1],sampRate)
    fbDict['hgamma'] = butter_bandpass_filter(data,f7[0],f7[1],sampRate)
    return(fbDict)

def waveletCon(sig, wavelet, widths):
    if not np.iscomplexobj(sig):
        sig=sig+0j
    
    for ind, width in enumerate(widths):
        wavelet_data = wavelet(min(10*width, len(sig)),width)
        wavelet_data_conj = np.conj(wavelet_data)
        output = signal.convolve(sig,wavelet_data_conj, mode='valid')
    return output

def doWaveAnalysis(sig,nChan):
    wavCoef=list()
    # transform=dtcwt.Transform1d()
    sigbuf=np.zeros(2048)
    sigLen=sig.shape[1]
    for chan in range(nChan):
        onedSig=sig[chan,:]
        wavelet = signal.morlet
        widths = np.array([1])
        wavc=waveletCon(onedSig, wavelet, widths)
        wavCoef.append(wavc)
        
    wavArray=np.array(wavCoef);
    return wavArray

def calcSPLVDict(wvDict):
    #splvDict=dict()
    subBandSPLV = np.array([])
    for key in wvDict.keys():
        nChan = wvDict[key].shape[0] 
        nPairs = (nChan * (nChan-1))/2
        outSplv = np.zeros(nPairs)
        pair = 0
        for chan in range(nChan-1):
            for chan2 in range(chan+1,nChan):
                wavc1 = wvDict[key][chan]
                wavc2 = wvDict[key][chan2]
                xcorr = wavc1 * np.conj(wavc2)
                w1abs = np.abs(wavc1)
                w2abs = np.abs(wavc2)
                normFact = w1abs * w2abs
                phaseSig = np.divide(xcorr, normFact)
                nlen = phaseSig.shape
                splv = np.abs(np.sum(phaseSig))/nlen
                outSplv[pair]= splv
                pair = pair +1
        subBandSPLV = np.hstack((subBandSPLV, outSplv))
        #splvDict[key] = outSplv
    return(subBandSPLV)


def wvOnFilterBank(fbDict):
    wvDict=dict()
    for key in fbDict.keys():
        nChan = fbDict[key].shape[0]
        wvDict[key]=doWaveAnalysis(fbDict[key],nChan)
    return wvDict

def loadFile(fileName):
    data=loadmat(fileName)#matlab data structure loaded as a python dict
    baseKey = [s for s in data.keys() if 'ictal' in s] #get the dict key or segment name:e.g.'interictal_segment_340'
    if len(baseKey) == 0:
        print "@@@@@@@ We've got an empty list on our hands. Which is expected. SKIPPING non-test segment!"
    else:
        bk = baseKey[0]
        dataArray=data[bk][0][0][0] #numpy ndarray nchan * nsamples
        numChan=dataArray.shape[0] #dim1 of data array
        numSamp=dataArray.shape[1] #dim2 of data array
        lenInSec=data[bk][0][0][1][0] #len of the segment in seconds
        sampRate=data[bk][0][0][2][0][0] # sampling rate in Hz
        electrodes=data[bk][0][0][3][0] #names of the electrodes as npy array
        # clipIndex=data[baseKey][0][0][4][0][0] #clipIndex
        return(dataArray,numChan,numSamp,sampRate,lenInSec)


def main(argv):
    fileName = ''
    outfileName = ''
    try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
      print 'waveletFeats.py -i <inputfile> -o <outputfile>'
      sys.exit(2)

    for opt, arg in opts:
      if opt == '-h':
         print 'waveletFeats.py -i <inputfile> -o <outputfile>'
         sys.exit()
      elif opt in ("-i", "--ifile"):
         fileName = arg[:-5]
      elif opt in ("-o", "--ofile"):
         outfileName = arg[:-4]
    print "Input file is:%s"%fileName
    print "Output file is:%s"%outfileName

    #"Dog_1/Dog_1_interictal_segment_0340.mat"
    (dataArray,numChan,numSamp,sampRate,lenInSec)=loadFile(fileName)
    winSize=10#window size in sec
    bufSize=int(np.ceil(sampRate*winSize))
    bufIdx=0
    numSeg=int(np.ceil(numSamp/bufSize))
    finalVec = np.array([])
    for nseg in range(numSeg):
        seg=dataArray[:,bufIdx:bufIdx+bufSize]
        fbDict = filterBankAnalysis(seg,sampRate)
        wvDict = wvOnFilterBank(fbDict)
        splvVec = calcSPLVDict(wvDict)
        finalVec = np.hstack((finalVec,splvVec))
        bufIdx=bufIdx+bufSize
    np.save(outfileName,finalVec)

if __name__=="__main__":
    try: 
        main(sys.argv[1:])
    except IndexError:
        print "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& IndexError excepted &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"
    except IOError: 
        print "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& IOError excepted &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"
    except MemoryError: 
        print "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& MemoryError excepted &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"
    except RuntimeWarning:
        print "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& RuntimeWarning excepted &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"
        sys.exit(2)


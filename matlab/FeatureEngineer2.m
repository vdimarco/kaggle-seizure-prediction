zfunction [preictalTrain, interIctalTrain, test] = FeatureEngineer2(x)
% Create a directory system for the files required
preictalClips = dir([x '*_preictal_*.mat']);
interIctalClips = dir([x '*_interictal_*.mat']);
testClips = dir([x '*_test_*.mat']);
file = load([x preictalClips(1).name]);
names = fieldnames(file);
thisFile = file.(names{1});
n = size(thisFile.data,1);
% Allocate memory for each loop to store the information
% including the Variances, Means, Correlations and latency
preictalTrain = zeros(length(preictalClips),n + n*(n-1)/2 + 1);
% Now perform feature extraction upon each individual file
parfor i = 1:length(preictalClips);
    % Load the file
    file = load([x preictalClips(i).name]);
    names = fieldnames(file);
    thisFile = file.(names{1});
    % Unpack the data and store the latency variable
    data = thisFile.data;
    % Now allocate a variable to store the extracted variances
    ChannelVariance = zeros(1,n);
    for j = 1:n;
        ChannelVariance(1,j) = var(data(j,:));
    end
    % Calculate the correlation coefficients between each of the n
    % channels
    CorrelationMatrix = corrcoef(data');
    % Now there are quite a few more EEG channels for the human data so I
    % won't be able to pull the same stunt I did with the dog data
    ChannelCorrelation = [];
    for k = 1:n;
        ChannelCorrelation = horzcat(ChannelCorrelation, CorrelationMatrix(k, k+1:n));
    end    
    % Now stitch the ChannelMean ChannelVariance and the ChannelCoefficient data with
    % the latency variable and store the data
    preictalTrain(i,:) = [1 ChannelVariance ChannelCorrelation];
    if i == length(preictalClips)
        disp('Generated Features from preictalClips')
    end
end

% Allocate memory for each loop to store the information
% including the Variances, Means, Correlations and latency
interIctalTrain = zeros(length(interIctalClips), n + n*(n-1)/2 +1);
% Now perform feature extraction upon each individual file
parfor i = 1:length(interIctalClips);
    % Load the file
    file = load([x interIctalClips(i).name]);
    names = fieldnames(file);
    thisFile = file.(names{1});
    % Unpack the data and store the latency variable
    data = thisFile.data;
    % Now allocate a variable to store the extracted variances
    ChannelVariance = zeros(1,n);
    for j = 1:n;
        ChannelVariance(1,j) = var(data(j,:));
    end
    % Calculate the correlation coefficients between each of the n
    % channels
    CorrelationMatrix = corrcoef(data');
    % Now there are quite a few more EEG channels for the human data so I
    % won't be able to pull the same stunt I did with the dog data
    ChannelCorrelation = [];
    for k = 1:n;
        ChannelCorrelation = horzcat(ChannelCorrelation, CorrelationMatrix(k, k+1:n));
    end    
    % Now stitch the ChannelMean ChannelVariance and the ChannelCoefficient data with
    % the latency variable and store the data
    interIctalTrain(i,:) = [0 ChannelVariance ChannelCorrelation];
    if i == length(interIctalClips)
        disp('Generated Features from interIctalClips')
    end
end

% Allocate memory for each loop to store the information
% including the Variances, Means, Correlations
test = zeros(length(testClips), n + n*(n-1)/2);
% Now perform feature extraction upon each individual file
parfor i = 1:length(testClips);
    % Load the file
    file = load([x testClips(i).name]);
    names = fieldnames(file);
    thisFile = file.(names{1});
    % Unpack the data and store the latency variable
    data = thisFile.data;
    % Now allocate a variable to store the extracted variances
    ChannelVariance = zeros(1,n);
    for j = 1:n;
        ChannelVariance(1,j) = var(data(j,:));
    end
    % Calculate the correlation coefficients between each of the n
    % channels
    CorrelationMatrix = corrcoef(data');
    % Now there are quite a few more EEG channels for the human data so I
    % won't be able to pull the same stunt I did with the dog data
    ChannelCorrelation = [];
    for k = 1:n;
        ChannelCorrelation = horzcat(ChannelCorrelation, CorrelationMatrix(k, k+1:n));
    end    
    % Now stitch the ChannelMean ChannelVariance and the ChannelCoefficient and store the data
    test(i,:) = [ChannelVariance ChannelCorrelation];
    if i == length(testClips);
        disp('Generated Features from testClips')
    end
end
end

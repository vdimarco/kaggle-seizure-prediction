http://www.kaggle.com/c/seizure-prediction/forums/t/10312/getting-started-code-by-elliot-dawson

So I noticed that roughly 33% of the leaderboard consists of people who have yet to beat the random benchmark of 0.5 and thought that since these people have made the effort to download the considerably large dataset I would lend a hand in providing some starter code. Not really sure if I'm going to have a great deal of time to work on this competition with my University schedule at the moment and given the positive externality that this competition intends to have I hope that this might provide someone else with a platform to produce a great algorithm.

This method achieves a public leaderboard score of around 0.635 and takes about 25 minutes to run using 6 workers in MATLAB (courtesy of the parallel computing toolbox) on a quad core i7 processor. My machine has 16GB of RAM and didn't have any memory issues when running the code. Having said that I'm unsure of how an 8GB RAM machine might cope. At the moment this approach works as follows:
- For each subject clip calculate : The variance of each channel as well as the correlation coefficient between each channel
- Take these values and use them as your predictor matrix
- For each subject train a decision tree using MATLAB's treeBagger with 1000 trees using the subjects' respective predictor matrix calculated from the clips
- Predict on each subjects' test data using their respective trained decision tree

Just run BenchmarkCode once you've added FeatureEngineer2.m to the path and changed the directories from where to write and read the data to and from.

On a separate note I used the 'table' data type to import the sample submission and to write the resulting submission to file which I believe was only introduced in MATLAB version 2013b but of course you can modify the code to write the result however you like :)

=====

Thanks for sharing. Similarly, a lasso logistic regression model with only variance of each feature does equally well at ~0.64. Try cvglmnet.m from glmnet toolbox. This is an open source version of Matlab's lassoglm.m  I ran on 8GB 2008 MacPro and had no problems with completion between 45-60 minutes 


Let me know if you're having any issues with the code.
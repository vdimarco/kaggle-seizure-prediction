import numpy as np
import cPickle as pickle
import os
import sys
from os.path import basename
import gc

#Loads a file from a list.
#Format of the list
#/fullpathtofile/file1.npy 0
#/fullpathtofile/file2.npy 1
def loadTrainFiles(fileList):
    try:
        fp=open(fileList,"r")
    except:
        print "There was an error opening:%s"%(fileList)
        sys.exit()

    trainFramesList = list()
    targetsList = list()
        
    for f in fp:
        f=f.strip()
        fname=f
        if not os.path.exists(fname):
            print "File %s does not exist"%(fname)
            continue
        else:
            if "interictal" in f:
                targ_label=0
            else:
                targ_label=1
            gc.disable()
            train_feat=np.load(fname)
            train_feat=train_feat.astype(np.float32)
            targ_label = int(targ_label)
            trainFramesList.append(train_feat)
            targetsList.append(targ_label)
            gc.enable()
    trNpy = np.array(trainFramesList)
    tarNpy = np.array(targetsList)
    return(trNpy,tarNpy)        

def loadTestFiles(fileList):
    testFramesList = list()
    try:
        fp=open(fileList,"r")
    except:
        print "There was an error opening:%s"%(fileList)
        sys.exit()
    for f in fp:
        f=f.strip()
       
        if not os.path.exists(f):
            print "File %s does not exist"%(f)
            continue
        else:
            test_feat=np.load(f)
            test_feat=test_feat.astype(np.float32)
            testFramesList.append(test_feat)
    teNpy = np.array(testFramesList)
    return(teNpy)

def loadTestFilesWithFeatList(fileList):
    testFramesList = list()
    try:
        fp=open(fileList,"r")
    except:
        print "There was an error opening:%s"%(fileList)
        sys.exit()
    featList=list()
    baseNameList=list()
    for f in fp:
        f=f.strip()
        print "Processing:%s"%f
        if not os.path.exists(f):
            print "File %s does not exist"%(f)
            continue
        else:
            test_feat=np.load(f)
            test_feat=test_feat.astype(np.float32)
            testFramesList.append(test_feat)
            bname=basename(f)
            bname=bname.split('.')[0]
            featList.append(bname)
            rootNameList=bname.split('_')
            rootName='_'.join(rootNameList[:-1])
            baseNameList.append(rootName)
    teNpy = np.array(testFramesList)
    fp.close()
    return(teNpy,featList,baseNameList)

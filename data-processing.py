#!/usr/bin/env python2.7

import waveletFeatures as wave
import numpy as np

import os, sys, getopt
from os import listdir
from os import walk
from os.path import isfile, join

targets = [
  'Dog_1',
  'Dog_2',
  'Dog_3',
  'Dog_4',
  'Dog_5',
  'Patient_1',
  'Patient_2'
]

segments = ['interictal', 'preictal', 'test']

# onlyfiles = [ f for f in listdir(data_folder) if isfile(join(data_folder,f)) ]

def main(argv):
  data_folder = "data"
  test = False
  try:
    opts, args = getopt.getopt(argv,"hd:t")
  except getopt.GetoptError:
    print "python <script_name.py> -d 'folder_path' [-t]"
    sys.exit(2)

  for opt, arg in opts:
    if opt == '-h':
      print "python <script_name.py> -d 'folder_path' [-t]"
      sys.exit()
    elif opt == '-d':
      data_folder = arg
    elif opt == '-t':
      test = True
      print "test set to true"
  print "Selected Data Folder is: %s" %data_folder

  for (dirpath, dirnames, filenames) in walk(data_folder):
    print "# scanning folder path: %s" %dirpath
    print "# subfolders are: %s" %dirnames
    print "# filenames are: %s" %filenames
    for f in filenames: 
      if f[-4:] == ".mat":
        if test is True and "test" in f:
          print "### checking if TEST .npy already exists for %s" %f
          if (f[:-4] +".npy") in filenames:
            print "#### TEST .npy already generated, skipping"
          else:
            command = join("python waveletFeaturesTest.py -i .", dirpath, f, " -o .", dirpath, f)
            print "#### TEST .npy not found, generating..."
            print "#### $ %s" %command
            os.system(command)
        elif test is False and "test" not in f:
          print "### checking if LABELED .npy already exists for %s" %f
          if (f[:-4] +".npy") in filenames:
            print "#### LABELED .npy already generated, skipping"
          else:
            command = join("python waveletFeatures.py -i .", dirpath, f, " -o .", dirpath, f)
            print "#### LABELED .npy not found, generating..."
            print "#### $ %s" %command
            os.system(command)
        else:
          print "### skipping file: %s" %f
      else:
        print "skipping file: %s" %f


if __name__=="__main__":
  main(sys.argv[1:])

import sys
from datetime import timedelta
from pandas.io.data import DataReader


class ProgressBar(object):
    '''
    Return A simple textual progress bar.
    Parameters
    ----------
    tasks_completed : int
        should be an incremented value to measure the progress of your tasks.
    total_tasks : int
        total number of tasks
    Retruns
    -------
    str :
        A progress bar like [#########  ]
    Example
    -------
    progress_bar = ProgressBar(2)
    def do_stuff():
        # do somethings
    progress_bar.update()
    >>>[# ]
    def do_more_things():
        # do so many things
    progress_bar.update()
    >>>[##]
    '''
    def __init__(self, total_tasks):
        self.total_tasks = total_tasks
        self.tasks_completed = 0
        self.progress = "\r["

    def increment(self):
        self.tasks_completed += 1

        for completed_task in xrange(0, self.tasks_completed):
            self.progress += "#"

        for incomplete_task in xrange(self.tasks_completed, self.total_tasks):
            self.progress += " "

    def show(self):
        sys.stdout.write(self.progress + "]")
        sys.stdout.flush()

    def update(self):
        self.increment()
        self.show()
"""
 Copyright (c) 2011,2012 George Dahl

 Permission is hereby granted, free of charge, to any person  obtaining
 a copy of this software and associated documentation  files (the
 "Software"), to deal in the Software without  restriction, including
 without limitation the rights to use,  copy, modify, merge, publish,
 distribute, sublicense, and/or sell  copies of the Software, and to
 permit persons to whom the  Software is furnished to do so, subject
 to the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.  THE
 SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT  HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,  WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING  FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR  OTHER DEALINGS IN THE
 SOFTWARE.
"""

import numpy as num
import itertools
from gdbn.dbn import *
from loadData import loadTrainFiles,loadTestFiles

def numMistakes(targetsMB, outputs):
    if not isinstance(outputs, num.ndarray):
        outputs = outputs.as_numpy_array()
    if not isinstance(targetsMB, num.ndarray):
        targetsMB = targetsMB.as_numpy_array()
    return num.sum(outputs.argmax(1) != targetsMB.argmax(1))

def sampleMinibatch(mbsz, inps, targs):
    idx = num.random.randint(inps.shape[0], size=(mbsz,))
    return inps[idx], targs[idx]

def main():
    mbsz = 64
    layerSizes = [96, 40, 2]
    scales = [0.05 for i in range(len(layerSizes)-1)]
    fanOuts = [None for i in range(len(layerSizes)-1)]
    learnRate = 0.00001
    epochs = 3
    
    
    listToLoad="Dog_1_tr.list"
    trainInps,trainTargs=loadTrainFiles(listToLoad)
    trainSize = trainInps.shape[0]
    mbPerEpoch = int(num.ceil(trainSize/mbsz))
    mbStream = (sampleMinibatch(mbsz, trainInps, trainTargs) for unused in itertools.repeat(None))
    
    net = buildDBN(layerSizes, scales, fanOuts, Softmax(), False)
    net.learnRates = [learnRate for x in net.learnRates]
    net.L2Costs = [0 for x in net.L2Costs]
    net.nestCompare = True #this flag existing is a design flaw that I might address later, for now always set it to True
    net.nTargets=2

    for ep, (trCE, trEr) in enumerate(net.fineTune(mbStream, epochs, mbPerEpoch, numMistakes, True)):
        print ep, trCE, trEr
   
    #trainPred=net.predictions(trainInps)
     
    testList="Dog_1_val.list"
    testInps,testTargs=loadTrainFiles(testList)
    outputs = tuple(net.predictions(testInps))

    #idx=0
    #fp=open(testList,"r")
    #for f in fp:
    #    print "%s,%d"%(f,num.argmax(outputs[idx]))
    #    idx=idx+1
    #fp.close()
    outputs = num.array(outputs).reshape(testInps.shape[0], -1)
    print "Test error rate:", numMistakes(testTargs, outputs) / float(testInps.shape[0])
    print "Done"

if __name__ == "__main__":
    main()

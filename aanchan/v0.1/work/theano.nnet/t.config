floatX (('float64', 'float32')) 
    Doc:  Default floating-point precision for python casts
    Value:  float64

cast_policy (('custom', 'numpy+floatX')) 
    Doc:  Rules for implicit type casting
    Value:  custom

int_division (('int', 'raise', 'floatX')) 
    Doc:  What to do when one computes x / y, where both x and y are of integer types
    Value:  int

device (cpu, gpu*, opencl*, cuda*) 
    Doc:  Default device for computations. If gpu*, change the default to try to move computation to it and to put shared variable of float32 on it. Do not use upper case letters, only lower case even if NVIDIA use capital letters.
    Value:  cpu

gpuarray.init_device (<type 'str'>) 
    Doc:  
             Device to initialize for gpuarray use without moving
             computations automatically.
             
    Value:  

init_gpu_device (('', 'gpu', 'gpu0', 'gpu1', 'gpu2', 'gpu3', 'gpu4', 'gpu5', 'gpu6', 'gpu7', 'gpu8', 'gpu9', 'gpu10', 'gpu11', 'gpu12', 'gpu13', 'gpu14', 'gpu15')) 
    Doc:  Initialize the gpu device to use, works only if device=cpu. Unlike 'device', setting this option will NOT move computations, nor shared variables, to the specified GPU. It can be used to run GPU-specific tests on a particular GPU.
    Value:  

force_device (<function booltype at 0x7f094eb32d70>) 
    Doc:  Raise an error if we can't use the specified device
    Value:  False

print_active_device (<function booltype at 0x7f094eb32ed8>) 
    Doc:  Print active device at when the GPU device is initialized.
    Value:  True

mode (('Mode', 'ProfileMode', 'DebugMode', 'FAST_RUN', 'FAST_COMPILE', 'PROFILE_MODE', 'DEBUG_MODE')) 
    Doc:  Default compilation mode
    Value:  Mode

linker (('cvm', 'c|py', 'py', 'c', 'c|py_nogc', 'c&py', 'vm', 'vm_nogc', 'cvm_nogc')) 
    Doc:  Default linker used if the theano flags mode is Mode or ProfileMode(deprecated)
    Value:  cvm

cxx (('g++', '')) 
    Doc:  The C++ compiler to use. Currently only g++ is supported, but supporting additional compilers should not be too difficult. If it is empty, no C++ code is compiled.
    Value:  g++

allow_gc (<function booltype at 0x7f094eb4a230>) 
    Doc:  Do we default to delete intermediate results during Theano function calls? Doing so lowers the memory requirement, but asks that we reallocate memory at the next function call. This is implemented for the default linker, but may not work for all linkers.
    Value:  True

optimizer (('fast_run', 'merge', 'fast_compile', 'None')) 
    Doc:  Default optimizer. If not None, will use this linker with the Mode object (not ProfileMode(deprecated) or DebugMode)
    Value:  fast_run

optimizer_verbose (<function booltype at 0x7f094eb4a410>) 
    Doc:  If True, we print all optimization being applied
    Value:  False

on_opt_error (('warn', 'raise', 'pdb')) 
    Doc:  What to do when an optimization crashes: warn and skip it, raise the exception, or fall into the pdb debugger.
    Value:  warn

<theano.configparser.ConfigParam object at 0x7f094eb45b50>
    Doc:  This config option was removed in 0.5: do not use it!
    Value:  True

nocleanup (<function booltype at 0x7f094eb4a668>) 
    Doc:  Suppress the deletion of code files that did not compile cleanly
    Value:  False

on_unused_input (('raise', 'warn', 'ignore')) 
    Doc:  What to do if a variable in the 'inputs' list of  theano.function() is not used in the graph.
    Value:  raise

tensor.cmp_sloppy (<type 'int'>) 
    Doc:  Relax tensor._allclose (0) not at all, (1) a bit, (2) more
    Value:  0

tensor.local_elemwise_fusion (<function booltype at 0x7f094eb4a938>) 
    Doc:  Enable or not in fast_run mode(fast_run optimization) the elemwise fusion optimization
    Value:  True

gpu.local_elemwise_fusion (<function booltype at 0x7f094eb4aaa0>) 
    Doc:  Enable or not in fast_run mode(fast_run optimization) the gpu elemwise fusion optimization
    Value:  True

lib.amdlibm (<function booltype at 0x7f094eb4ac08>) 
    Doc:  Use amd's amdlibm numerical library
    Value:  False

gpuelemwise.sync (<function booltype at 0x7f094eb4ad70>) 
    Doc:  when true, wait that the gpu fct finished and check it error code.
    Value:  True

traceback.limit (<type 'int'>) 
    Doc:  The number of stack to trace. -1 mean all.
    Value:  5

experimental.mrg (<function booltype at 0x7f094eb4af50>) 
    Doc:  Another random number generator that work on the gpu
    Value:  False

numpy.seterr_all (('ignore', 'warn', 'raise', 'call', 'print', 'log', 'None')) 
    Doc:  ("Sets numpy's behaviour for floating-point errors, ", "see numpy.seterr. 'None' means not to change numpy's default, which can be different for different numpy releases. This flag sets the default behaviour for all kinds of floating-point errors, its effect can be overriden for specific errors by the following flags: seterr_divide, seterr_over, seterr_under and seterr_invalid.")
    Value:  ignore

numpy.seterr_divide (('None', 'ignore', 'warn', 'raise', 'call', 'print', 'log')) 
    Doc:  Sets numpy's behavior for division by zero, see numpy.seterr. 'None' means using the default, defined by numpy.seterr_all.
    Value:  None

numpy.seterr_over (('None', 'ignore', 'warn', 'raise', 'call', 'print', 'log')) 
    Doc:  Sets numpy's behavior for floating-point overflow, see numpy.seterr. 'None' means using the default, defined by numpy.seterr_all.
    Value:  None

numpy.seterr_under (('None', 'ignore', 'warn', 'raise', 'call', 'print', 'log')) 
    Doc:  Sets numpy's behavior for floating-point underflow, see numpy.seterr. 'None' means using the default, defined by numpy.seterr_all.
    Value:  None

numpy.seterr_invalid (('None', 'ignore', 'warn', 'raise', 'call', 'print', 'log')) 
    Doc:  Sets numpy's behavior for invalid floating-point operation, see numpy.seterr. 'None' means using the default, defined by numpy.seterr_all.
    Value:  None

warn.ignore_bug_before (('0.5', 'None', 'all', '0.3', '0.4', '0.4.1', '0.6')) 
    Doc:  If 'None', we warn about all Theano bugs found by default. If 'all', we don't warn about Theano bugs found by default. If a version, we print only the warnings relative to Theano bugs found after that version. Warning for specific bugs can be configured with specific [warn] flags.
    Value:  0.5

warn.argmax_pushdown_bug (<function booltype at 0x7f094eb4d488>) 
    Doc:  Warn if in past version of Theano we generated a bug with the theano.tensor.nnet.nnet.local_argmax_pushdown optimization. Was fixed 27 may 2010
    Value:  False

warn.gpusum_01_011_0111_bug (<function booltype at 0x7f094eb4d5f0>) 
    Doc:  Warn if we are in a case where old version of Theano had a silent bug with GpuSum pattern 01,011 and 0111 when the first dimensions was bigger then 4096. Was fixed 31 may 2010
    Value:  False

warn.sum_sum_bug (<function booltype at 0x7f094eb4d758>) 
    Doc:  Warn if we are in a case where Theano version between version 9923a40c7b7a and the 2 august 2010 (fixed date), generated an error in that case. This happens when there are 2 consecutive sums in the graph, bad code was generated. Was fixed 2 August 2010
    Value:  False

warn.sum_div_dimshuffle_bug (<function booltype at 0x7f094eb4d8c0>) 
    Doc:  Warn if previous versions of Theano (between rev. 3bd9b789f5e8, 2010-06-16, and cfc6322e5ad4, 2010-08-03) would have given incorrect result. This bug was triggered by sum of division of dimshuffled tensors.
    Value:  False

warn.subtensor_merge_bug (<function booltype at 0x7f094eb4da28>) 
    Doc:  Warn if previous versions of Theano (before 0.5rc2) could have given incorrect results when indexing into a subtensor with negative stride (for instance, for instance, x[a:b:-1][c]).
    Value:  False

warn.gpu_set_subtensor1 (<function booltype at 0x7f094eb4db90>) 
    Doc:  Warn if previous versions of Theano (before 0.6) could have given incorrect results when moving to the gpu set_subtensor(x[int vector], new_value)
    Value:  True

warn.vm_gc_bug (<function booltype at 0x7f094eb4dcf8>) 
    Doc:  There was a bug that existed in the default Theano configuration, only in the development version between July 5th 2012 and July 30th 2012. This was not in a released version. If your code was affected by this bug, a warning will be printed during the code execution if you use the `linker=vm,vm.lazy=True,warn.vm_gc_bug=True` Theano flags. This warning is disabled by default as the bug was not released.
    Value:  False

compute_test_value (('off', 'ignore', 'warn', 'raise', 'pdb')) 
    Doc:  If 'True', Theano will run each op at graph build time, using Constants, SharedVariables and the tag 'test_value' as inputs to the function. This helps the user track down problems in the graph before it gets optimized.
    Value:  off

compute_test_value_opt (('off', 'ignore', 'warn', 'raise', 'pdb')) 
    Doc:  For debugging Theano optimization only. Same as compute_test_value, but is used during Theano optimization
    Value:  off

exception_verbosity (('low', 'high')) 
    Doc:  If 'low', the text of exceptions will generally refer to apply nodes with short names such as Elemwise{add_no_inplace}. If 'high', some exceptions will also refer to apply nodes with long descriptions  like:
        A. Elemwise{add_no_inplace}
                B. log_likelihood_v_given_h
                C. log_likelihood_h
    Value:  low

openmp (<function booltype at 0x7f094eb52050>) 
    Doc:  Allow (or not) parallel computation on the CPU with OpenMP. This is the default value used when creating an Op that supports OpenMP parallelization. It is preferable to define it via the Theano configuration file ~/.theanorc or with the environment variable THEANO_FLAGS. Parallelization is only done for some operations that implement it, and even for operations that implement parallelism, each operation is free to respect this flag or not. You can control the number of threads used with the environment variable OMP_NUM_THREADS. If it is set to 1, we disable openmp in Theano by default.
    Value:  False

gcc.cxxflags (<type 'str'>) 
    Doc:  Extra compiler flags for gcc
    Value:  

compiledir_format (<type 'str'>) 
    Doc:  Format string for platform-dependent compiled module subdirectory
(relative to base_compiledir). Available keys: gxx_version, hostname,
numpy_version, platform, processor, python_bitwidth,
python_int_bitwidth, python_version, theano_version. Defaults to 'comp
iledir_%(platform)s-%(processor)s-%(python_version)s-%(python_bitwidth
)s'.
    Value:  compiledir_%(platform)s-%(processor)s-%(python_version)s-%(python_bitwidth)s

<theano.configparser.ConfigParam object at 0x7f094a044950>
    Doc:  platform-independent root directory for compiled modules
    Value:  /home/azureuser/.theano

<theano.configparser.ConfigParam object at 0x7f094a044c90>
    Doc:  platform-dependent cache directory for compiled modules
    Value:  /home/azureuser/.theano/compiledir_Linux-3.13.0-36-generic-x86_64-with-debian-jessie-sid-x86_64-2.7.8-64

cmodule.mac_framework_link (<function booltype at 0x7f094a053cf8>) 
    Doc:  If set to True, breaks certain MacOS installations with the infamous Bus Error
    Value:  False

cmodule.warn_no_version (<function booltype at 0x7f094a053e60>) 
    Doc:  If True, will print a warning when compiling one or more Op with C code that can't be cached because there is no c_code_cache_version() function associated to at least one of those Ops.
    Value:  False

cmodule.remove_gxx_opt (<function booltype at 0x7f094a057050>) 
    Doc:  If True, will remove the -O* parameter passed to g++.This is useful to debug in gdb modules compiled by Theano.The parameter -g is passed by default to g++
    Value:  False

cmodule.compilation_warning (<function booltype at 0x7f094a0571b8>) 
    Doc:  If True, will print compilation warnings.
    Value:  False

optdb.position_cutoff (<type 'float'>) 
    Doc:  Where to stop eariler during optimization. It represent the position of the optimizer where to stop.
    Value:  inf

optdb.max_use_ratio (<type 'float'>) 
    Doc:  A ratio that prevent infinite loop in EquilibriumOptimizer.
    Value:  5.0

profile (<function booltype at 0x7f0949dcc938>) 
    Doc:  If VM should collect profile information
    Value:  False

profile_optimizer (<function booltype at 0x7f0949dcc9b0>) 
    Doc:  If VM should collect optimizer profile information
    Value:  False

profile_memory (<function booltype at 0x7f0949dccd70>) 
    Doc:  If VM should collect memory profile information and print it
    Value:  False

<theano.configparser.ConfigParam object at 0x7f0949d66790>
    Doc:  Useful only for the vm linkers. When lazy is None, auto detect if lazy evaluation is needed and use the apropriate version. If lazy is True/False, force the version used between Loop/LoopGC and Stack.
    Value:  None

optimizer_excluding (<type 'str'>) 
    Doc:  When using the default mode, we will remove optimizer with these tags. Separate tags with ':'.
    Value:  

optimizer_including (<type 'str'>) 
    Doc:  When using the default mode, we will add optimizer with these tags. Separate tags with ':'.
    Value:  

optimizer_requiring (<type 'str'>) 
    Doc:  When using the default mode, we will require optimizer with these tags. Separate tags with ':'.
    Value:  

DebugMode.patience (<type 'int'>) 
    Doc:  Optimize graph this many times to detect inconsistency
    Value:  10

DebugMode.check_c (<function booltype at 0x7f0949b2e320>) 
    Doc:  Run C implementations where possible
    Value:  True

DebugMode.check_py (<function booltype at 0x7f0949b2e050>) 
    Doc:  Run Python implementations where possible
    Value:  True

DebugMode.check_finite (<function booltype at 0x7f0949b2ae60>) 
    Doc:  True -> complain about NaN/Inf results
    Value:  True

DebugMode.check_strides (<type 'int'>) 
    Doc:  Check that Python- and C-produced ndarrays have same strides.  On difference: (0) - ignore, (1) warn, or (2) raise error
    Value:  1

DebugMode.warn_input_not_reused (<function booltype at 0x7f0949b2ab18>) 
    Doc:  Generate a warning when destroy_map or view_map says that an op works inplace, but the op did not reuse the input for its output.
    Value:  True

DebugMode.check_preallocated_output (<type 'str'>) 
    Doc:  Test thunks with pre-allocated memory as output storage. This is a list of strings separated by ":". Valid values are: "initial" (initial storage in storage map, happens with Scan),"previous" (previously-returned memory), "c_contiguous", "f_contiguous", "strided" (positive and negative strides), "wrong_size" (larger and smaller dimensions), and "ALL" (all of the above).
    Value:  

DebugMode.check_preallocated_output_ndim (<type 'int'>) 
    Doc:  When testing with "strided" preallocated output memory, test all combinations of strides over that number of (inner-most) dimensions. You may want to reduce that number to reduce memory or time usage, but it is advised to keep a minimum of 2.
    Value:  4

profiling.time_thunks (<function booltype at 0x7f0949b54758>) 
    Doc:  Time individual thunks when profiling
    Value:  True

profiling.n_apply (<type 'int'>) 
    Doc:  Number of Apply instances to print by default
    Value:  20

profiling.n_ops (<type 'int'>) 
    Doc:  Number of Ops to print by default
    Value:  20

profiling.min_memory_size (<type 'int'>) 
    Doc:  For the memory profile, do not print Apply nodes if the size
             of their outputs (in bytes) is lower than this threshold
    Value:  1024

ProfileMode.n_apply_to_print (<type 'int'>) 
    Doc:  Number of apply instances to print by default
    Value:  15

ProfileMode.n_ops_to_print (<type 'int'>) 
    Doc:  Number of ops to print by default
    Value:  20

ProfileMode.min_memory_size (<type 'int'>) 
    Doc:  For the memory profile, do not print apply nodes if the size
 of their outputs (in bytes) is lower then this threshold
    Value:  1024

ProfileMode.profile_memory (<function booltype at 0x7f0949ae16e0>) 
    Doc:  Enable profiling of memory used by Theano functions
    Value:  False

on_shape_error (('warn', 'raise')) 
    Doc:  warn: print a warning and use the default value. raise: raise an error
    Value:  warn

tensor.insert_inplace_optimizer_validate_nb (<type 'int'>) 
    Doc:  -1: auto, if graph have less then 500 nodes 1, else 10
    Value:  -1

experimental.local_alloc_elemwise (<function booltype at 0x7f0941655aa0>) 
    Doc:  If True enable the experimental optimization local_alloc_elemwise
    Value:  False

experimental.local_alloc_elemwise_assert (<function booltype at 0x7f0941652398>) 
    Doc:  If False enable the experimental optimization local_alloc_elemwise but WITHOUT assert into the graph!
    Value:  True

blas.ldflags (<type 'str'>) 
    Doc:  lib[s] to include for [Fortran] level-3 blas implementation
    Value:  -L/home/azureuser/anaconda/lib -lf77blas -lcblas -latlas

warn.identify_1pexp_bug (<function booltype at 0x7f09406622a8>) 
    Doc:  Warn if Theano versions prior to 7987b51 (2011-12-18) could have yielded a wrong result due to a bug in the is_1pexp function
    Value:  False

unittests.rseed (<type 'str'>) 
    Doc:  Seed to use for randomized unit tests. Special value 'random' means using a seed of None.
    Value:  666



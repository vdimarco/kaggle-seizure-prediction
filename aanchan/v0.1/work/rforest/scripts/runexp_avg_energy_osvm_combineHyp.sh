#!/bin/bash


#python randomForest.py -i ~/kaggle_seizure/aanchan/v0.1/work/lists/Dog_4_train.list -t ~/kaggle_seizure/aanchan/v0.1/work/lists/Dog_4_test.list -r True -o $PWD


DATA_DIR=$HOME/kaggle_seizure/data
WORK_DIR=$HOME/kaggle_seizure/aanchan/v0.1/work/rforest/scripts
EXP_DIR=$HOME/kaggle_seizure/aanchan/v0.1/work/oneclassSVM/exp
LIST_DIR=$HOME/kaggle_seizure/aanchan/v0.1/work/lists/avg_energy
EXP_NAME=avg_energy

mkdir -p $EXP_DIR/$EXP_NAME

#ls -l $DATA_DIR  | grep "^d" | awk '{print $NF}'
#$(ls -l $DATA_DIR  | grep "^d" | awk '{print $NF}')

for target in Dog_1;do
    echo "python $WORK_DIR/one-class-svm.py -i $LIST_DIR/${target}_train.list -t $LIST_DIR/${target}_val.list -r True -o $EXP_DIR/$EXP_NAME"
done

#Dog_1_tr.list
#Dog_1_val.list

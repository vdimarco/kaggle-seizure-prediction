#!/bin/bash


#python randomForest.py -i ~/kaggle_seizure/aanchan/v0.1/work/lists/Dog_4_train.list -t ~/kaggle_seizure/aanchan/v0.1/work/lists/Dog_4_test.list -r True -o $PWD


DATA_DIR=$HOME/kaggle_seizure/data
WORK_DIR=$HOME/kaggle_seizure/aanchan/v0.1/work/rforest/scripts
EXP_DIR=$HOME/kaggle_seizure/aanchan/v0.1/work/rforest/exp
LIST_DIR=$HOME/kaggle_seizure/aanchan/v0.1/work/lists/avg_energy
EXP_NAME=avg_energy_Dog2_100

mkdir -p $EXP_DIR/$EXP_NAME

#ls -l $DATA_DIR  | grep "^d" | awk '{print $NF}'

for target in Dog_2;do
    python $WORK_DIR/randomForest.py -i $LIST_DIR/${target}_tr.list -t $LIST_DIR/${target}_tr.list -r True -o $EXP_DIR/$EXP_NAME    
done

#Dog_1_tr.list
#Dog_1_val.list

from sklearn.ensemble import RandomForestClassifier
import numpy as np
import cPickle as pickle
import os,sys
import loadData
import getopt


def train(trBatch,tarBatch):
    clf = RandomForestClassifier(n_estimators=100, max_depth=None, min_samples_split=1,n_jobs=1)
    clf.fit(trBatch,tarBatch)
    return clf
    
    
   
def processCommandLine(argv):
    fileName = ''
    testfileName = ''
    mode = False
    outDir = ''
    try:
      opts, args = getopt.getopt(argv,"hi:t:r:o:",["ifile=","test=","result=","outdir="])
    except getopt.GetoptError:
      print 'randomForest.py -i <testfile> -t <testfile> -r <train mode/test mode> -o <outdir>'
      sys.exit(2)
    for opt, arg in opts:
      if opt == '-h':
         print 'randomForest.py -i <testfile> -t <testfile> -r <train mode/test mode> -o <outdir>'
         sys.exit()
      elif opt in ("-i", "--ifile"):
         fileName = arg
      elif opt in ("-t", "--test"):
         testfileName = arg
      elif opt in ("-r", "--result"):
          if arg in "True":
              mode = True
          else:
              mode = False
      elif opt in ("-o","--outdir"):
          outDir = arg
          if not (os.path.exists(outDir)):
              os.makedirs(outDir)

    print "Input file is:%s"%fileName
    print "Output file is:%s"%testfileName
    return (fileName,testfileName,mode,outDir)

################################################
#CombineHypothesis
#fileList: a list containing names of 1min segments
#resultList: a list containing the results of the prediction on 1min segments
#baseNameList: a list containing names of the root 10min segments

#################################################
def combineHyp(fileList,resultList,baseNameList):
    dictHyp = dict(zip(fileList, resultList)) #create a dictionary with keys as 1min segment names and 0/1 results as values
    dictResult=dict()
    for bname in baseNameList:#Loop over 10min segment names
        keyList=list()
        for s in range(1,10):    #create 1 min segment names and store in list keyList
            key = "%s_%s"%(bname,s)
            keyList.append(key)
        resList=[dictHyp[key] for key in keyList] # get all the results for each 1 min segment corresponding to the bname file
        resArray=np.array(resList) #create an array from the result list
        #combine here: (a) just the max
        dictResult[bname]=np.max(resArray) #combine results
    return dictResult

def main(argv):
    (trainFile, testFile,mode,outDir)=processCommandLine(argv)
    if mode == False:
        trainFlag=True
        testFlag=False
    else:
        testFlag=True
        trainFlag=False
    subject="%s_%s"%(trainFile.split('/')[-1].split('_')[0],trainFile.split('/')[-1].split('_')[1])
    print "Subject is %s"%(subject)
    if trainFlag:
        trdata,trtarg=loadData.loadTrainFiles(trainFile)
        clf=train(trdata,trtarg)
        fp1=open("%s/model.%s"%(outDir,subject),"wb")
        pickle.dump(clf,fp1)
        fp1.close()
    
    if testFlag:
        fp2=open("%s/model.%s"%(outDir,subject),"rb")
        clf=pickle.load(fp2) # this is the sklearn classifier
        fp2.close()

        #######################################################################
        #Comments to understand the following code
        #
        #testFile is a list of fileNames that looks like
        #/home/azureuser/kaggle_seizure/aanchan/v0.1/feats/avg_energy_test/Dog_2/Dog_2_test_segment_0104_8.npy
        #/home/azureuser/kaggle_seizure/aanchan/v0.1/feats/avg_energy_test/Dog_2/Dog_2_test_segment_0104_8.npy
        # ....
        #
        #1.loadData.loadTestFileWithFeatList takes *testFile* as an argument
        #  and returns:
        #       (a)testdata=numpy array of all the test vectors
        #       (b)fileList = a list of corresponding file names e.g. Dog_2_test_segment_0104_8
        #       (c)rootNameList = a list containing rootfile names e.g. Dog_2_test_segment_0104
        #
        #  (a) and (b) contain the same number of elements of 1 min test segments. 
        #  (c) contains as many elements as there are 10 min full test .mat files
        #2. result=clf.predict(testdata) outputs predictions of the model
        #3. the "result" numpy array is converted to a list
        #4. "fileList", " rootNameList", and "resultList" are handed off to combineHyp which returns 
        #    a dictionary with keys like "Dog_2_test_segment_0104" with the combined Hypothesis.
        #######################################################################################
        (testdata,fileList,rootNameList)=loadData.loadTestFilesWithFeatList(testFile)
        
        result = clf.predict(testdata)
        resultList=result.tolist()
        dictCombHyp=combineHyp(fileList,resultList,rootNameList)
        fp3=open("%s/res.%s"%(outDir,subject),"wb")
        pickle.dump(dictCombHyp,fp3)
        fp3.close()

if __name__=="__main__":
   main(sys.argv[1:])

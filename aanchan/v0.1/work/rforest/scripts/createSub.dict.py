import cPickle as pickle

targetList=["Dog_1","Dog_2","Dog_3","Dog_4","Dog_5","Patient_1","Patient_2"]

listDir="/home/azureuser/kaggle_seizure/aanchan/v0.1/work/lists/avg_energy"
expDir="/home/azureuser/kaggle_seizure/aanchan/v0.1/work/rforest/exp/avg_energy_logreg"

print "clip,preictal"
for sub in targetList:
    fp=open("%s/res.%s"%(expDir,sub),"rb")
    resDict=pickle.load(fp)
    fp.close()
    fp=open("%s/%s_test.list"%(listDir,sub))
    idx=0
    
    for testSeg in resDict.keys():
        testSeg=testSeg.strip()
        testSeg=testSeg.split('/')[-1]
        #testSeg=testSeg.split('.')[0]
        print "%s.mat,%d"%(testSeg,resDict[testSeg])
        idx+=1

from sklearn.ensemble import RandomForestClassifier
import numpy as np
import cPickle as pickle
import os,sys
import loadData
import getopt


def train(trBatch,tarBatch):
    clf = RandomForestClassifier(n_estimators=100, max_depth=None, min_samples_split=1,n_jobs=1)
    clf.fit(trBatch,tarBatch)
    return clf
    
    
   
def processCommandLine(argv):
    fileName = ''
    testfileName = ''
    mode = False
    outDir = ''
    try:
      opts, args = getopt.getopt(argv,"hi:t:r:o:",["ifile=","test=","result=","outdir="])
    except getopt.GetoptError:
      print 'randomForest.py -i <testfile> -t <testfile> -r <train mode/test mode> -o <outdir>'
      sys.exit(2)
    for opt, arg in opts:
      if opt == '-h':
         print 'randomForest.py -i <testfile> -t <testfile> -r <train mode/test mode> -o <outdir>'
         sys.exit()
      elif opt in ("-i", "--ifile"):
         fileName = arg
      elif opt in ("-t", "--test"):
         testfileName = arg
      elif opt in ("-r", "--result"):
          if arg in "True":
              mode = True
          else:
              mode = False
      elif opt in ("-o","--outdir"):
          outDir = arg
          if not (os.path.exists(outDir)):
              os.makedirs(outDir)

    print "Input file is:%s"%fileName
    print "Output file is:%s"%testfileName
    return (fileName,testfileName,mode,outDir)

def main(argv):
    (trainFile, testFile,mode,outDir)=processCommandLine(argv)
    if mode == False:
        trainFlag=True
        testFlag=False
    else:
        testFlag=True
        trainFlag=False
    subject="%s_%s"%(trainFile.split('/')[-1].split('_')[0],trainFile.split('/')[-1].split('_')[1])
    print "Subject is %s"%(subject)
    if trainFlag:
        trdata,trtarg=loadData.loadTrainFiles(trainFile)
        clf=train(trdata,trtarg)
        fp1=open("%s/model.%s"%(outDir,subject),"wb")
        pickle.dump(clf,fp1)
        fp1.close()
    
    if testFlag:
        fp2=open("%s/model.%s"%(outDir,subject),"rb")
        clf=pickle.load(fp2)
        fp2.close()
        testdata=loadData.loadTestFiles(testFile)
        result = clf.predict(testdata)
        #combine results

        fp3=open("%s/res.%s"%(outDir,subject),"wb")
        pickle.dump(result,fp3)
        fp3.close()

if __name__=="__main__":
   main(sys.argv[1:])

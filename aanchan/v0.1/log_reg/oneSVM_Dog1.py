from sklearn.covariance import EllipticEnvelope
import numpy as np
import cPickle as pickle
import os

clf = EllipticEnvelope()

fp=open("dog_4_train.list","r")

files=fp.readlines()
nExamples=len(files)
batchSize=nExamples
nBatches=nExamples/batchSize
lastBatchSize=nExamples-nBatches*batchSize
batchNum=0


def getNextBatch(batchNum,batchSize,files):
    stIdx=batchNum*batchSize
    enIdx=(batchNum+1)*batchSize
    fBatch = files[stIdx:enIdx]
    trainList = list()
    outList = list()
    trainTarg = list()
    outTarg = list()
    for idx in range(batchSize):
        if not os.path.exists(fBatch[idx].strip().split(' ')[0]):
            continue
        else:
            f=fBatch[idx]
        f=f.strip()
        fname, targ_label=f.split(' ')
        train_feat=np.load(fname)
        targ_label = int(targ_label)
        if targ_label == 0:
            targ_label = -1
            trainList.append(train_feat)
            trainTarg.append(targ_label)
        else:
            outList.append(train_feat)
            outTarg.append(targ_label)
        
        
    trBatch = np.array(trainList)
    outBatch= np.array(outList)
    trainTarg = np.array(trainTarg)
    outTar = np.array(outTarg)
    return trBatch,trainTarg, outBatch, outTar


def train():
    for batch in range(nBatches):
        trBatch, trainTarg,outBatch, outTarg = getNextBatch(batch,batchSize,files)
        clf.fit(trBatch)
        batch=batch+1

    testList="dog_4.test.list"
    flist=open(testList,"r")
    flistLines = flist.readlines()
    testFrames = list()
    for f in flistLines:
        f=f.strip()
        testFrame=np.load(f)
        testFrames.append(testFrame)
    testFramesNpy = np.array(testFrames)
    result = clf.predict(testFramesNpy)
   
    #clf.predict(trBatch)
    #process last batch
    #trBatch,tarBatch = processLastBatch(lastBatchSize,files)
    #clf.fit(trBatch,tarBatch)
    #fp=open("dog_1.model","wb")
    #pickle.dump(clf,fp)
    #fp.close()


if __name__=="__main__":
    train()

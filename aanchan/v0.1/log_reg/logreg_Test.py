from sklearn.linear_model import SGDClassifier
import numpy as np
import cPickle as pickle


if __name__ == "__main__":
    fp=open("dog_1.model","rb")
    clf = pickle.load(fp)
    testList="dog_1.test.list"
    flist=open(testList,"r")
    testFrames = list()
    for f in flist:
        f=f.strip()
        testFrame=np.load(f)
        prediction=clf.predict(testFrame)
        print "%s %d\n"%(f,prediction[0])
    flist.close()

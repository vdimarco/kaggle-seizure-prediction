
from scipy.io import loadmat
import numpy as np
from sklearn import preprocessing 
import sys, getopt
from scipy import signal


def loadFile(fileName):
    data=loadmat(fileName)#matlab data structure loaded as a python dict
    baseKey=[s for s in data.keys() if "test" in s]#get the dict key or segment name:e.g.'interictal_segment_340'
    baseKey = baseKey[0]
    dataArray=data[baseKey][0][0][0] #numpy ndarray nchan * nsamples
    numChan=dataArray.shape[0] #dim1 of data array
    numSamp=dataArray.shape[1] #dim2 of data array
    lenInSec=data[baseKey][0][0][1][0] #len of the segment in seconds
    sampRate=data[baseKey][0][0][2][0][0] # sampling rate in Hz
    electrodes=data[baseKey][0][0][3][0] #names of the electrodes as npy array
    #clipIndex=data[baseKey][0][0][4][0][0] #clipIndex
    return(dataArray,numChan,numSamp,sampRate,lenInSec)

 
def fft(time_data):
    return np.log10(np.absolute(np.fft.rfft(time_data, axis=1)[:,1:48]))

def freq_corr(fft_data):
    scaled = preprocessing.scale(fft_data, axis=0)
    corr_matrix = np.corrcoef(scaled)
    eigenvalues = np.absolute(np.linalg.eig(corr_matrix)[0])
    eigenvalues.sort()
    ind = corr_matrix.shape[0]
    iu1 = np.triu_indices(ind)
    corr_coefficients = corr_matrix[iu1] # custom func
    return np.concatenate((corr_coefficients, eigenvalues))



def time_corr(time_data):
    resampled = signal.resample(time_data, 400, axis=1) \
        if time_data.shape[-1] > 400 else time_data
    scaled = preprocessing.scale(resampled, axis=0)
    corr_matrix = np.corrcoef(scaled)
    eigenvalues = np.absolute(np.linalg.eig(corr_matrix)[0])
    ind = corr_matrix.shape[0]
    iu1 = np.triu_indices(ind)
    corr_coefficients = corr_matrix[iu1] # custom func
    return np.concatenate((corr_coefficients, eigenvalues))

def transform(data):
    fft_out = fft(data)
    freq_corr_out = freq_corr(fft_out)
    time_corr_out = time_corr(data)
    return np.concatenate((fft_out.ravel(), freq_corr_out, time_corr_out))


def main(argv):
    fileName = ''
    outfileName = ''
    try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
      print 'mhillsfft.py -i <inputfile> -o <outputfile>'
      sys.exit(2)
    for opt, arg in opts:
      if opt == '-h':
         print 'mhillsfft.py -i <inputfile> -o <outputfile>'
         sys.exit()
      elif opt in ("-i", "--ifile"):
         fileName = arg
      elif opt in ("-o", "--ofile"):
         outfileName = arg
    print "Input file is:%s"%fileName
    print "Output file is:%s"%outfileName

    #"Dog_1/Dog_1_interictal_segment_0340.mat"
    (dataArray,numChan,numSamp,sampRate,lenInSec)=loadFile(fileName)
    winSize=10#window size in sec
    bufSize=int(np.ceil(sampRate*winSize))
    bufIdx=0
    numSeg=int(np.ceil(numSamp/bufSize))
    finalVec = np.array([])
    for nseg in range(numSeg):
        seg=dataArray[:,bufIdx:bufIdx+bufSize]
        intVec=transform(seg)
        finalVec = np.hstack((finalVec,intVec))
        bufIdx=bufIdx+bufSize
    np.save(outfileName,finalVec)

if __name__=="__main__":
    main(sys.argv[1:])
    

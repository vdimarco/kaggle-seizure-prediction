# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

from scipy.io import loadmat
# import dtcwt
import numpy as np
from scipy.signal import butter,lfilter
from scipy import signal
import sys, getopt
# <codecell>

def getfbpsd(data,sampRate):
    f1 = [0.1,4] #delta filter bank
    f2 = [4,7] #theta filter bank
    f3 = [8,12] # alpha filter bank
    f4 = [12,30] # high beta
    f5 = [30,70] # low gamma
    f6 = [70, 180] #high gamma
    
    N=data.shape[1]
    xfft=fft(data)#16*nsamp-matrix
    psd=(2*np.square(xfft))/(N*sampRate)#this is OK since across b1..b6 taking only positive frequencies
    #psd=10*np.log10(psd)# take a log to compress dynamic range
    invFreqRes=N/sampRate # use this to get the correct dft bin for a given freq f 
    
    idx1=int(np.ceil(f1[0]*invFreqRes))
    idx2=int(np.ceil(f1[1]*invFreqRes))
    b1=np.sum(psd[:,idx1:idx2],axis=1)#each b is a 16 chan vector
    idx1=int(np.ceil(f2[0]*invFreqRes))
    idx2=int(np.ceil(f2[1]*invFreqRes))
    b2=np.sum(psd[:,idx1:idx2],axis=1)
    idx1=int(np.ceil(f3[0]*invFreqRes))
    idx2=int(np.ceil(f3[1]*invFreqRes))
    b3=np.sum(psd[:,idx1:idx2],axis=1)
    idx1=int(np.ceil(f4[0]*invFreqRes))
    idx2=int(np.ceil(f4[1]*invFreqRes))
    b4=np.sum(psd[:,idx1:idx2],axis=1)
    idx1=int(np.ceil(f5[0]*invFreqRes))
    idx2=int(np.ceil(f5[1]*invFreqRes))
    b5=np.sum(psd[:,idx1:idx2],axis=1)
    idx1=int(np.ceil(f6[0]*invFreqRes))
    idx2=int(np.ceil(f6[1]*invFreqRes))
    b6=np.sum(psd[:,idx1:idx2],axis=1)
    psdvec=np.hstack((b1.ravel(),b2.ravel(),b3.ravel(),b4.ravel(),b5.ravel(),b6.ravel()))
    return(psdvec)


def fft(time_data):
    return (np.absolute(np.fft.rfft(time_data, axis=1)))


def loadFile(fileName):
    data=loadmat(fileName)#matlab data structure loaded as a python dict
    baseKey = [s for s in data.keys() if 'ictal' in s] #get the dict key or segment name:e.g.'interictal_segment_340'
    if len(baseKey) == 0:
        print "@@@@@@@ We've got an empty list on our hands. Which is expected. SKIPPING non-test segment!"
    else:
        bk = baseKey[0]
        dataArray=data[bk][0][0][0] #numpy ndarray nchan * nsamples
        numChan=dataArray.shape[0] #dim1 of data array
        numSamp=dataArray.shape[1] #dim2 of data array
        lenInSec=data[bk][0][0][1][0] #len of the segment in seconds
        sampRate=data[bk][0][0][2][0][0] # sampling rate in Hz
        electrodes=data[bk][0][0][3][0] #names of the electrodes as npy array
        # clipIndex=data[baseKey][0][0][4][0][0] #clipIndex
        return(dataArray,numChan,numSamp,sampRate,lenInSec)


def main(argv):
    fileName = ''
    outfileName = ''
    try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
      print 'enegy_fft.py -i <inputfile> -o <outputfile>'
      sys.exit(2)

    for opt, arg in opts:
      if opt == '-h':
         print 'energy_fft.py -i <inputfile> -o <outputfile>'
         sys.exit()
      elif opt in ("-i", "--ifile"):
         fileName = arg
         #arg[:-5]
      elif opt in ("-o", "--ofile"):
         outfileName = arg
         #arg[:-4]
    print "Input file is:%s"%fileName
    print "Output file is:%s"%outfileName

    #"Dog_1/Dog_1_interictal_segment_0340.mat"
    (dataArray,numChan,numSamp,sampRate,lenInSec)=loadFile(fileName)
    winSize=60#window size in sec
    bufSize=int(np.ceil(sampRate*float(winSize)))
    fftSize = int(np.power(2, np.ceil(np.log(bufSize)/np.log(2))))
    bufIdx=0
    numSeg=int(np.ceil(numSamp/float(bufSize)))
    finalVec = np.array([])
    fftBuf= np.zeros(numChan*fftSize)
    fftBuf = fftBuf.reshape(numChan,fftSize)
    for nseg in range(numSeg):
        print "Segment Index:%d"%(numSeg)
        seg=dataArray[:,bufIdx:bufIdx+bufSize]
        segLen=seg.shape[1]
        fftBuf[:,:segLen]=seg
        fbpsdvec=getfbpsd(fftBuf,sampRate)#calculate psd over 
        #multiple channels, concatenate as vectors
        np.save("%s_%d"%(outfileName,nseg+1),fbpsdvec.astype(np.float32))
        fftBuf= np.zeros(numChan*fftSize)
        fftBuf = fftBuf.reshape(numChan,fftSize)
        bufIdx=bufIdx+bufSize
    

if __name__=="__main__":
    try: 
        main(sys.argv[1:])
    except IndexError:
        print "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& IndexError excepted &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"
    except IOError: 
        print "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& IOError excepted &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"
    except MemoryError: 
        print "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& MemoryError excepted &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"
    except RuntimeWarning:
        print "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& RuntimeWarning excepted &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"
        sys.exit(2)


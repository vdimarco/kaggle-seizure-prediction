#!/bin/bash

TOP_DIR=/home/azureuser/kaggle_seizure/data
DATA_DIR=/home/azureuser/kaggle_seizure/aanchan/v0.1/feats
FFTDIR=avg_energy
#train list
if false;then
for target in $(ls -l $TOP_DIR  | grep "^d" | awk '{print $NF}');do
    for l in $(ls $DATA_DIR/$FFTDIR/$target | grep npy | grep -v train);do 
	echo $DATA_DIR/$FFTDIR/$target/$l 
    done | shuf > /home/azureuser/kaggle_seizure/aanchan/v0.1/work/lists/${target}_train.list
done
fi

FFTDIR=avg_energy_test
#test list
if true;then
for target in $(ls -l $TOP_DIR  | grep "^d" | awk '{print $NF}');do
    for l in $(ls $DATA_DIR/$FFTDIR/$target | grep npy | grep test);do 
	echo $DATA_DIR/$FFTDIR/$target/$l 
    done | shuf > /home/azureuser/kaggle_seizure/aanchan/v0.1/work/lists/${target}_test.list
done
fi



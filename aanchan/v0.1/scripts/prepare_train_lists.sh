#!/bin/bash


DATA_DIR=/home/azureuser/kaggle_seizure/data
#FFTDIR=mhillsfft-dp
#train list
if false;then
for target in $(ls -l $DATA_DIR  | grep "^d" | awk '{print $NF}');do
    for l in $(ls $DATA_DIR/$target | grep mat | grep -v npy | grep -v test);do 
	echo $DATA_DIR/$target/$l 
    done | shuf > ../work/lists/train-lists/${target}_train.list
done
fi

#test list
if true;then
for target in $(ls -l $DATA_DIR  | grep "^d" | awk '{print $NF}');do
    for l in $(ls $DATA_DIR/$target | grep mat | grep -v npy | grep test);do 
	echo $DATA_DIR/$target/$l 
    done | shuf > ../work/lists/test-lists/${target}_test.list
done
fi



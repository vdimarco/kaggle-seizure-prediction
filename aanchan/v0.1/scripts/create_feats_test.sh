#!/bin/bash 


TAR=$1
DATA_DIR=/home/azureuser/kaggle_seizure/data
OUTDIR=/home/azureuser/kaggle_seizure/aanchan/v0.1/feats
FEATTYPE=avg-energy-test
FEATDIR=$OUTDIR/$FEATTYPE/$TAR
TRAINLIST_DIR=/home/azureuser/kaggle_seizure/aanchan/v0.1/work/lists/test-lists
mkdir -p $FEATDIR


for target in $TAR;do
    trainList=$TRAINLIST_DIR/${target}_test.list
    for fileName in $(<$trainList);do
	fname=$(basename $fileName)
	fname_noextension=$FEATDIR/${fname%.*}
	python energy_fft_avg_test.py -i $fileName -o $fname_noextension
    done
done

#This gets a list of all targets from the data directory
#$(ls -l $DATA_DIR  | grep "^d" | awk '{print $NF}')

from sklearn.linear_model import LogisticRegression
import numpy as np
import cPickle as pickle
clf = LogisticRegression()

fp=open("dog_1_train.list","r")

files=fp.readlines()
nExamples=len(files)
batchSize=nExamples
nBatches=nExamples/batchSize
lastBatchSize=nExamples-nBatches*batchSize
batchNum=0

def getNextBatch(batchNum,batchSize,files):
    stIdx=batchNum*batchSize
    enIdx=(batchNum+1)*batchSize
    fBatch = files[stIdx:enIdx]
    trainFramesList = list()
    targetsList = list()
    for f in fBatch:
        f=f.strip()
        fname, targ_label=f.split(' ')
        train_feat=np.load(fname)
        targ_label = int(targ_label)
        trainFramesList.append(train_feat)
        targetsList.append(targ_label)
    trBatch = np.array(trainFramesList)
    tarBatch = np.array(targetsList)
    return trBatch,tarBatch

def processLastBatch(lastBatchSize,files):
    lastBatchIdx=len(files)-lastBatchSize
    fBatch=files[lastBatchIdx:]
    trainFramesList = list()
    targetsList = list()
    for f in fBatch:
        f=f.strip()
        fname, targ_label=f.split(' ')
        train_feat=np.load(fname)
        targ_label = int(targ_label)
        trainFramesList.append(train_feat)
        targetsList.append(targ_label)
    trBatch = np.array(trainFramesList)
    tarBatch = np.array(targetsList)
    return trBatch,tarBatch

def train():
    for batch in range(nBatches):
        trBatch,tarBatch = getNextBatch(batchNum,batchSize,files)
        clf.fit(trBatch,tarBatch)
        batch=batch+1
    #process last batch
    #trBatch,tarBatch = processLastBatch(lastBatchSize,files)
    #clf.fit(trBatch,tarBatch)
    fp=open("dog_1.model","wb")
    pickle.dump(clf,fp)
    fp.close()


if __name__=="__main__":
    train()

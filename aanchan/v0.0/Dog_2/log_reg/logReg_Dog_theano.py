
import numpy as np
import cPickle as pickle
import theano
import theano.tensor as T
rng = np.random


fp=open("dog_2.train.list","r")

files=fp.readlines()
nExamples=len(files)
batchSize=nExamples
nBatches=nExamples/batchSize
lastBatchSize=nExamples-nBatches*batchSize
batchNum=0

def getNextBatch(batchNum,batchSize,files):
    stIdx=batchNum*batchSize
    enIdx=(batchNum+1)*batchSize
    fBatch = files[stIdx:enIdx]
    trainFramesList = list()
    targetsList = list()
    for f in fBatch:
        f=f.strip()
        fname, targ_label=f.split(' ')
        train_feat=np.load(fname)
        targ_label = int(targ_label)
        trainFramesList.append(train_feat)
        targetsList.append(targ_label)
    trBatch = np.array(trainFramesList)
    tarBatch = np.array(targetsList)
    return trBatch,tarBatch

def processLastBatch(lastBatchSize,files):
    lastBatchIdx=len(files)-lastBatchSize
    fBatch=files[lastBatchIdx:]
    trainFramesList = list()
    targetsList = list()
    for f in fBatch:
        f=f.strip()
        fname, targ_label=f.split(' ')
        train_feat=np.load(fname)
        targ_label = int(targ_label)
        trainFramesList.append(train_feat)
        targetsList.append(targ_label)
    trBatch = np.array(trainFramesList)
    tarBatch = np.array(targetsList)
    return trBatch,tarBatch

def train():
    
    for batch in range(nBatches):
        train_data,train_supervision = getNextBatch(batchNum,batchSize,files)
        batch=batch+1

        
    D=(train_data,train_supervision)
    nrows = train_data.shape[0]
    ncols=train_data.shape[1]
    feats=ncols
    training_steps = 2


    x = T.matrix("x")
    y = T.vector("y")
    w = theano.shared(rng.randn(feats), name="w")
    b = theano.shared(0., name="b")
    scale = theano.function([], w, updates=[(w, w*0.0001)])
    print "Initial model:"
    scale()
    print w.get_value(), b.get_value()

    # Construct Theano expression graph
    p_1 = 1 / (1 + T.exp(-T.dot(x, w) - b))   # Probability that target = 1
    prediction = p_1 > 0.5                    # The prediction thresholded
    xent = -y * T.log(p_1) - (1-y) * T.log(1-p_1) # Cross-entropy loss function
    cost = xent.mean() + 0.0001 * (w ** 2).sum()# The cost to minimize
    gw, gb = T.grad(cost, [w, b])             # Compute the gradient of the cost
                                          # (we shall return to this in a
                                          # following section of this tutorial)
    # Compile
    train = theano.function(inputs=[x,y],outputs=[prediction, xent],updates=((w, w - 0.001 * gw), (b, b - 0.001 * gb)))
    predict = theano.function(inputs=[x], outputs=prediction)

    # Train
    for i in range(training_steps):
        pred, err = train(D[0], D[1])
        print "%d %f"%(i ,np.sum(err)/nrows)
    print "Final model:"
    print w.get_value(), b.get_value()

    #Test
    testList="dog_2.test.list"
    flist=open(testList,"r")
    flistLines = flist.readlines()
    testFrames = list()
    flistTemp=flistLines[:858]
    for f in flistTemp:
        f=f.strip()
        testFrame=np.load(f)
        testFrames.append(testFrame)
    testFramesNpy = np.array(testFrames)
    result = predict(testFramesNpy)
    

#    fp=open("dog_1.model","wb")
#    pickle.dump(clf,fp)
#    fp.close()


if __name__=="__main__":
    train()

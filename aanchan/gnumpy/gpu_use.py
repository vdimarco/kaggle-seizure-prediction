import os
import atexit
import sys
gpuLockDir="/dev/shm"
lfN="gpu_lock"
_dev_prefix = '/dev/nvidia'

def board_ids():
    """Returns integer board ids available on this machine."""
    """Stole this from gpu_lock.py"""
    from glob import glob
    board_devs = glob(_dev_prefix + '[0-9]*')
    return range(len(board_devs))

def createLockFile():
    bId=os.environ.get('GNUMPY_USE_BOARD', '0')
    bId=int(bId)
    if findOwner(bId):
        print "Board ID:%d in use"%(bId)
        for bId in board_ids():
            if not findOwner(bId):
                print "Assigning Board ID:%d"%(bId)
                gpuLockFname=os.path.join(gpuLockDir,"%s_%d"%(lfN,int(bId)))
                fp=open(gpuLockFname,"w")
                fp.close()
                return gpuLockFname
        ####
        #If this point is reached no gpu boards are available
        ###
        print "No free gpu boards available, sorry!"        
        sys.exit(1)        
    else:
        gpuLockFname=os.path.join(gpuLockDir,"%s_%d"%(lfN,int(bId)))
        fp=open(gpuLockFname,"w")
        fp.close()
        return gpuLockFname

def deleteLockFile(name):
    os.remove(name)

def findOwner(bId):
    from os import stat
    from pwd import getpwuid
    lfName="%s/%s_%d"%(gpuLockDir,lfN,int(bId))
    try:
        owner=getpwuid(stat(lfName).st_uid).pw_name
    except:
        owner=None
    return owner

def printOwner(bId,owner):
    print("NVIDIA BOARD:%d OWNER:%s"%(int(bId),owner))

def printGpuUserStatus():
    for bId in board_ids():
        owner=findOwner(bId)
        printOwner(bId,owner)

def main():
    lfName=createLockFile()
    atexit.register(deleteLockFile,lfName)

if __name__=="__main__":
    printGpuUserStatus()
else:
    main()
    

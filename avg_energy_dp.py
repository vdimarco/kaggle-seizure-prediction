#!/usr/bin/env python2.7

import numpy as np

import os, sys, getopt
from os import listdir
from os import walk
from os.path import isfile, join

targets = [
  'Dog_1',
  'Dog_2',
  'Dog_3',
  'Dog_4',
  'Dog_5',
  'Patient_1',
  'Patient_2'
]

segments = ['interictal', 'preictal', 'test']


def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]


def main(argv):
  sname = os.path.basename(__file__)[:-6] # removes "-dp.py" suffix
  data_folder = "data"
  data_out = "aanchan/v0.1/feats/" + sname
  test = False
  helper = "python <script_name.py> -d 'path/to/input/folder' [-o 'path/to/output/folder'] [-t]"
  try:
    opts, args = getopt.getopt(argv,"d:oth")
  except getopt.GetoptError:
    print helper
    sys.exit(2)

  for opt, arg in opts:
    if opt == '-h':
      print helper
      sys.exit()
    elif opt == '-d':
      data_folder = arg
    elif opt == '-o':
      data_out = arg
    elif opt == '-t':
      test = True
      data_out = data_out + "_test"
      print "test set to true"
  print "Input data folder: %s" %data_folder
  print "Output data folder: %s" %data_out

  script_path = "~/kaggle_seizure/aanchan/v0.1/scripts/energy_fft_avg"
  for (dirpath, dirnames, files_in) in walk(data_folder):
    print "# scanning folder path: %s" %dirpath
    print "# subfolders are: %s" %dirnames
    print "# files_in are: %s" %files_in
    for f in files_in: 
      if f[-4:] == ".mat":
        if not os.path.exists(data_out):
          os.makedirs(data_out)
        if test is True and "test" in f:
          print "### checking if TEST .npy already exists for %s" %f
          files_out = [ fo for fo in listdir(data_out) if isfile(join(data_out,fo)) ] # look up output files
          if ((f +".npy") or (f[:-4] +".npy")) in files_out:
            print "#### TEST .npy already generated, skipping"
          else:
            command = "python %s_test_von.py -i %s/%s -o %s/%s" % (script_path, dirpath, f, data_out, f)
            print "#### TEST .npy not found, generating..."
            print "#### $ %s" %command
            os.system(command)
        elif test is False and "test" not in f:
          print "### checking if LABELED .npy already exists for %s" %f
          files_out = [ fo for fo in listdir(data_out) if isfile(join(data_out,fo)) ] # loop up output files
          if ((f[:-4] +".npy")) in files_out:
            print "#### LABELED .npy already generated, skipping"
          else:
            command = "python %s_von.py -i %s/%s -o %s/%s" % (script_path, dirpath, f, data_out, f)
            print "#### LABELED .npy not found, generating..."
            print "#### $ %s" %command
            os.system(command)
        else:
          print "### skipping file: %s" %f
      else:
        print "skipping file: %s" %f


if __name__=="__main__":
  main(sys.argv[1:])

#!/usr/bin/env python2.7

import subprocess   
import pprint
import os, sys, getopt
from os import listdir
from os import walk
from os.path import isfile, join

targets = [
  'Dog_1',
  'Dog_2',
  'Dog_3',
  'Dog_4',
  'Dog_5',
  'Patient_1',
  'Patient_2'
]

segments = ['interictal', 'preictal', 'test']

helper = "python <script_name.py> -d 'path/to/files'"


def main(argv):
  ls = "ls -1 "
  path = "data"
  wc = " | wc -l"
  try:
    opts, args = getopt.getopt(argv,"d:h")
  except getopt.GetoptError:
    print helper
    sys.exit(2)

  for opt, arg in opts:
    if opt == '-h':
      print helper
      sys.exit()
    elif opt == '-d':
      path = arg
  print "data folder set to: %s" %path
  print "##########################################"
  print "# NPY		# MAT		# Folder" 
  print "##########################################"
  for (dirpath, dirnames, filenames) in walk(path):
    # cmd = ls + dirpath + "/*mat" + wc
    # # mat = os.popen(cmd).read()
    # mat = subprocess.check_output(cmd, shell=True)
    # # print cmd
    # cmd = ls + dirpath + "/*npy" + wc
    # # npy = os.popen(cmd).read()
    # npy = subprocess.check_output(cmd, shell=True)
    # print cmd

    npy =  [f for f in filenames if ".npy" in f]
    mat =  [f for f in filenames if ".mat" in f]


    print "# %05d		# %05d		# %s" % (len(npy), len(mat), dirpath)

    # print "###### Found: " + npy[:-1] + " npy files, " + mat[:-1] + " mat files"

if __name__=="__main__":
  main(sys.argv[1:])



